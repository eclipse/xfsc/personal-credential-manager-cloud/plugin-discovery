package main

import (
	"log"

	serviceCore "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
	serviceTypes "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/services/plugins"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/plugin-discovery/types"
)

func main() {
	env := types.GetEnvironment()
	err := serviceCore.InitializeService("api", env)
	services := make([]serviceTypes.Service, 0)
	services = append(services, new(plugins.PluginsService))
	serviceCore.RegisterServices(services)
	if err == nil {
		err = serviceCore.StartService()
		if err != nil {
			log.Fatalf("unexpected error, %v", err)
		}
	} else {
		log.Fatalf("unexpected error, %v", err)
	}
}
